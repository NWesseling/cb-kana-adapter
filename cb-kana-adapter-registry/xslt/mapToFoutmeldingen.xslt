<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"	
	xmlns:if="http://nl.consumentenbond.services/adapters/KanaAdapterService/v1/IF">

	<!-- <xsl:template match="jsonObject[//code!=''] | jsonElement[//code!='']"> -->
	<xsl:param name="wso2foutmeldingcode"/>
	
	<xsl:template match="/">
	   <if:foutmeldingen>
	       <xsl:choose>
	           <xsl:when test="//meldingen[type='ERROR']/code">
	               <xsl:apply-templates select="//meldingen[type='ERROR']/code" />
	           </xsl:when>
	           <xsl:when test="$wso2foutmeldingcode != ''">
	               <xsl:call-template name="maakFoutmelding"/>
	           </xsl:when>
	           <xsl:otherwise>
	               <xsl:call-template name="maakFoutmelding"/>
	           </xsl:otherwise>
	       </xsl:choose>
                       
        </if:foutmeldingen>
	</xsl:template>
	
	<xsl:template match="code">
		<xsl:call-template name="maakFoutmelding">
		  <xsl:with-param name="code" select="."/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="maakFoutmelding">
	   <xsl:param name="code"/>	   
	   
	   <if:foutmelding>
            <xsl:choose>
                <xsl:when test="$code = 'CB-10000'">
                    <if:code>100</if:code>
                    <if:omschrijving>De validatie van invoergegevens is gefaalt</if:omschrijving>
                </xsl:when>
                <xsl:when test="$code = 'CB-10001'">
                    <if:code>101</if:code>
                    <if:omschrijving>Relatie is niet gevonden met opgegeven relatienummer</if:omschrijving>
                </xsl:when>                
                <xsl:when test="$code = ('CB-10003', 'CB-10004', 'CB-10007') ">
                    <if:code>104</if:code>
                    <if:omschrijving>Het verzoek tot registratie kan niet worden uitgevoerd aangezien dit tot mogelijk dubbeling leidt</if:omschrijving>
                </xsl:when>
                <xsl:when test="$code = ('CV-10012')">
                    <if:code>101</if:code>
                    <if:omschrijving>Opgegeven IBAN ongeldig is niet conform gehanteerde controles</if:omschrijving>
                </xsl:when>
                <xsl:when test="$code = ('CB-10013')">
                    <if:code>102</if:code>
                    <if:omschrijving>Tussenvoegsel onbekend</if:omschrijving>
                </xsl:when>                
                <xsl:when test="$code = ('CB-9000')">
                    <if:code>900</if:code>
                    <if:omschrijving>Er zijn verbindingsproblemen geconstateerd bij het verbinding naar interne services of applicaties</if:omschrijving>
                </xsl:when>                
                <xsl:when test="$code = ('CB-99999')">
                    <if:code>999</if:code>
                    <if:omschrijving>Er is een onbekende fout opgetreden. Neem contact op met de beheerder van de integratielaag voor verdere analyse</if:omschrijving>
                </xsl:when>                                                
                <xsl:otherwise>
                    <if:code>999</if:code>
                    <if:omschrijving>Er is een onbekende fout opgetreden. Neem contact op met de beheerder van de integratielaag voor verdere analyse.</if:omschrijving>
                </xsl:otherwise>
            </xsl:choose>
            <if:meldingType>ERROR</if:meldingType>
        </if:foutmelding>
	</xsl:template>	
</xsl:stylesheet>