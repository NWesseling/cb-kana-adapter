<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:if="http://nl.consumentenbond.services/adapters/KanaAdapterService/v1/IF">
	
	<!-- TODO naam xsl correcter maken -->
	<xsl:template match="if:relatie">
	   <relatie>
	       <!-- TODO juiste code invullen -->
	       <herkomst><xsl:text>A02</xsl:text></herkomst>
	       <xsl:apply-templates select="node()"/>
	   </relatie>
	</xsl:template>
	
	<xsl:template match="if:*">
        <xsl:element name="{lower-case(local-name())}">
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="if:adressen">
        <xsl:for-each select="if:adres">
            <xsl:processing-instruction name="xml-multiple"/>
            <adressen>
                <xsl:apply-templates select="node()"/>
            </adressen>
        </xsl:for-each>
    </xsl:template>
        
        
    <!-- TODO of dit aanpassen in de xsd? -->
    <xsl:template match="if:adrestype">
        <type>
            <xsl:value-of select="."/>
        </type>
    </xsl:template>
    
    <xsl:template match="if:contactgegevens">
        <xsl:if test="if:contactgegeven[if:contacttype = 'TEL_PRIMAIR'] != ''">
        	<telefoon>
        		<xsl:value-of select="if:contactgegeven[if:contacttype = 'TEL_PRIMAIR']/telefoon"/>
        	</telefoon>
        </xsl:if>        
    </xsl:template>
    
    <xsl:template match="if:telefoon">
    	<xsl:if test="if:typeTelefoon = 'TEL_PRIMAIR'">
    		<telefoon><xsl:value-of select="if:telefoonnummer"/></telefoon>
    	</xsl:if>
    </xsl:template>
    
    <!-- TODO zowel contactKanalen als contactKanaal zijn een multiple, hoe zit dit?? -->
    <xsl:template match="if:contactkanalen">
        <xsl:for-each select="if:contactkanaal">
            <xsl:processing-instruction name="xml-multiple"/>
            <contactkanalen>                
                <contactkanaal>
                    <xsl:apply-templates select="node()"/>
                </contactkanaal>
            </contactkanalen>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="if:postbus|if:panel|if:advertentieVrij|if:korting|if:incasso">
        <xsl:element name="{lower-case(local-name())}">
            <xsl:call-template name="mapBooleanToCDM">
                <xsl:with-param name="boolValue" select="." />
            </xsl:call-template>
        </xsl:element>
    </xsl:template>
    
    <!-- TODO betere naamgeving -->
    <xsl:template name="mapBooleanToCDM">
        <xsl:param name="boolValue"/>
        
        <xsl:choose>
            <xsl:when test="$boolValue='true'">
                <xsl:text>JA</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>NEE</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>